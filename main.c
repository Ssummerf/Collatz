/*
 * Simple Megaman Game
 */

#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 160

/* include the background image we are using */
#include <stdio.h>
#include "background.h"

//Setting up some sprites, and maps
#include "megaman.h"
#include "map2.h"
#include "map3.h"
#include "map.h"

#include "battletheme.h"
#include "damagery.h"
#include "sword.h"

/* the tile mode flags needed for display control register */
#define MODE0 0x00
#define BG0_ENABLE 0x100
#define BG1_ENABLE 0x200
#define BG2_ENABLE 0x400
#define BG3_ENABLE 0x800

#define SPRITE_MAP_2D 0x0
#define SPRITE_MAP_1D 0x40
#define SPRITE_ENABLE 0x1000

#define PALETTE_SIZE 256
#define NUM_SPRITES 128

volatile unsigned short* bg0_control = (volatile unsigned short*) 0x4000008;
volatile unsigned long* display_control = (volatile unsigned long*) 0x4000000;
volatile unsigned short* sprite_attribute_memory = (volatile unsigned short*) 0x7000000;
volatile unsigned short* sprite_image_memory = (volatile unsigned short*) 0x6010000;
volatile unsigned short* bg_palette = (volatile unsigned short*) 0x5000000;
volatile unsigned short* sprite_palette = (volatile unsigned short*) 0x5000200;
volatile unsigned short* buttons = (volatile unsigned short*) 0x04000130;

/* scrolling registers for backgrounds */
volatile short* bg0_x_scroll = (unsigned short*) 0x4000010;
volatile short* bg0_y_scroll = (unsigned short*) 0x4000012;

#define BUTTON_A (1 << 0)
#define BUTTON_B (1 << 1)
#define BUTTON_SELECT (1 << 2)
#define BUTTON_START (1 << 3)
#define BUTTON_RIGHT (1 << 4)
#define BUTTON_LEFT (1 << 5)
#define BUTTON_UP (1 << 6)
#define BUTTON_DOWN (1 << 7)
#define BUTTON_R (1 << 8)
#define BUTTON_L (1 << 9)

volatile unsigned short* scanline_counter = (volatile unsigned short*) 0x4000006;
volatile unsigned short* timer0_data = (volatile unsigned short*) 0x4000100;
volatile unsigned short* timer0_control = (volatile unsigned short*) 0x4000102;

#define TIMER_FREQ_1 0x0
#define TIMER_FREQ_64 0x2
#define TIMER_FREQ_256 0x3
#define TIMER_FREQ_1024 0x4
#define TIMER_ENABLE 0x80

#define CLOCK 16777216 
#define CYCLES_PER_BLANK 280806


void wait_vblank() {
    /* wait until all 160 lines have been updated */
    while (*scanline_counter < 160) { }
}

//Button Checking
unsigned char button_pressed(unsigned short button) {
    unsigned short pressed = *buttons & button;

    if (pressed == 0) {
        return 1;
    } else {
        return 0;
    }
}

volatile unsigned short* char_block(unsigned long block) {
    return (volatile unsigned short*) (0x6000000 + (block * 0x4000));
}

volatile unsigned short* screen_block(unsigned long block) {
    return (volatile unsigned short*) (0x6000000 + (block * 0x800));
}

/* flag for turning on DMA */
#define DMA_ENABLE 0x80000000

/* flags for the sizes to transfer, 16 or 32 bits */
#define DMA_16 0x00000000
#define DMA_32 0x04000000



/* this causes the DMA destination to be the same each time rather than increment */
#define DMA_DEST_FIXED 0x400000

/* this causes the DMA to repeat the transfer automatically on some interval */
#define DMA_REPEAT 0x2000000

/* this causes the DMA repeat interval to be synced with timer 0 */
#define DMA_SYNC_TO_TIMER 0x30000000


volatile unsigned int* dma1_source = (volatile unsigned int*) 0x40000BC;
volatile unsigned int* dma1_destination = (volatile unsigned int*) 0x40000C0;
volatile unsigned int* dma1_control = (volatile unsigned int*) 0x40000C4;

volatile unsigned int* dma2_source = (volatile unsigned int*) 0x40000C8;
volatile unsigned int* dma2_destination = (volatile unsigned int*) 0x40000CC;
volatile unsigned int* dma2_control = (volatile unsigned int*) 0x40000D0;

//original dma
volatile unsigned int* dma3_source = (volatile unsigned int*) 0x40000D4;
volatile unsigned int* dma3_destination = (volatile unsigned int*) 0x40000D8;
volatile unsigned int* dma3_control = (volatile unsigned int*) 0x40000DC;

void memcpy16_dma(unsigned short* dest, unsigned short* source, int amount) {
    *dma3_source = (unsigned int) source;
    *dma3_destination = (unsigned int) dest;
    *dma3_control = DMA_ENABLE | DMA_16 | amount;
}


/* the global interrupt enable register */
volatile unsigned short* interrupt_enable = (unsigned short*) 0x4000208;

/* this register stores the individual interrupts we want */
volatile unsigned short* interrupt_selection = (unsigned short*) 0x4000200;

/* this registers stores which interrupts if any occured */
volatile unsigned short* interrupt_state = (unsigned short*) 0x4000202;

/* the address of the function to call when an interrupt occurs */
volatile unsigned int* interrupt_callback = (unsigned int*) 0x3007FFC;

/* this register needs a bit set to tell the hardware to send the vblank interrupt */
volatile unsigned short* display_interrupts = (unsigned short*) 0x4000004;

/* the interrupts are identified by number, we only care about this one */
#define INTERRUPT_VBLANK 0x1

/* allows turning on and off sound for the GBA altogether */
volatile unsigned short* master_sound = (volatile unsigned short*) 0x4000084;
#define SOUND_MASTER_ENABLE 0x80

/* has various bits for controlling the direct sound channels */
volatile unsigned short* sound_control = (volatile unsigned short*) 0x4000082;

/* bit patterns for the sound control register */
#define SOUND_A_RIGHT_CHANNEL 0x100
#define SOUND_A_LEFT_CHANNEL 0x200
#define SOUND_A_FIFO_RESET 0x800
#define SOUND_B_RIGHT_CHANNEL 0x1000
#define SOUND_B_LEFT_CHANNEL 0x2000
#define SOUND_B_FIFO_RESET 0x8000

/* the location of where sound samples are placed for each channel */
volatile unsigned char* fifo_buffer_a  = (volatile unsigned char*) 0x40000A0;
volatile unsigned char* fifo_buffer_b  = (volatile unsigned char*) 0x40000A4;

/* global variables to keep track of how much longer the sounds are to play */
unsigned int channel_a_vblanks_remaining = 0;
unsigned int channel_b_vblanks_remaining = 0;


/* play a sound with a number of samples, and sample rate on one channel 'A' or 'B' */
void play_sound(const signed char* sound, int total_samples, int sample_rate, char channel) {
    /* start by disabling the timer and dma controller (to reset a previous sound) */
    *timer0_control = 0;
    if (channel == 'A') {
        *dma1_control = 0;
    } else if (channel == 'B') {
        *dma2_control = 0; 
    }

    /* output to both sides and reset the FIFO */
    if (channel == 'A') {
        *sound_control |= SOUND_A_RIGHT_CHANNEL | SOUND_A_LEFT_CHANNEL | SOUND_A_FIFO_RESET;
    } else if (channel == 'B') {
        *sound_control |= SOUND_B_RIGHT_CHANNEL | SOUND_B_LEFT_CHANNEL | SOUND_B_FIFO_RESET;
    }

    /* enable all sound */
    *master_sound = SOUND_MASTER_ENABLE;

    /* set the dma channel to transfer from the sound array to the sound buffer */
    if (channel == 'A') {
        *dma1_source = (unsigned int) sound;
        *dma1_destination = (unsigned int) fifo_buffer_a;
        *dma1_control = DMA_DEST_FIXED | DMA_REPEAT | DMA_32 | DMA_SYNC_TO_TIMER | DMA_ENABLE;
    } else if (channel == 'B') {
        *dma2_source = (unsigned int) sound;
        *dma2_destination = (unsigned int) fifo_buffer_b;
        *dma2_control = DMA_DEST_FIXED | DMA_REPEAT | DMA_32 | DMA_SYNC_TO_TIMER | DMA_ENABLE;
    }

    /* set the timer so that it increments once each time a sample is due
     * we divide the clock (ticks/second) by the sample rate (samples/second)
     * to get the number of ticks/samples */
    unsigned short ticks_per_sample = CLOCK / sample_rate;

    /* the timers all count up to 65536 and overflow at that point, so we count up to that
     * now the timer will trigger each time we need a sample, and cause DMA to give it one! */
    *timer0_data = 65536 - ticks_per_sample;

    /* determine length of playback in vblanks
     * this is the total number of samples, times the number of clock ticks per sample,
     * divided by the number of machine cycles per vblank (a constant) */
    if (channel == 'A') {
        channel_a_vblanks_remaining = total_samples * ticks_per_sample * (1.0 / CYCLES_PER_BLANK);
    } else if (channel == 'B') {
        channel_b_vblanks_remaining = total_samples * ticks_per_sample * (1.0 / CYCLES_PER_BLANK);
    }

    /* enable the timer */
    *timer0_control = TIMER_ENABLE | TIMER_FREQ_1;
}

/* this function is called each vblank to get the timing of sounds right */
void on_vblank() {
    /* disable interrupts for now and save current state of interrupt */
    *interrupt_enable = 0;
    unsigned short temp = *interrupt_state;

    /* look for vertical refresh */
    if ((*interrupt_state & INTERRUPT_VBLANK) == INTERRUPT_VBLANK) {

        /* update channel A */
        if (channel_a_vblanks_remaining == 0) {

            *sound_control = 0;
        }
        else {
            channel_a_vblanks_remaining--;
        }

        /* update channel B */
        if (channel_b_vblanks_remaining == 0) {
            /* disable the sound and DMA transfer on channel B */
            *sound_control &= ~(SOUND_B_RIGHT_CHANNEL | SOUND_B_LEFT_CHANNEL | SOUND_B_FIFO_RESET);
            *dma2_control = 0;
        }
        else {
            channel_b_vblanks_remaining--;
        }
    }

    /* restore/enable interrupts */
    *interrupt_state = temp;
    *interrupt_enable = 1;
}


/* Modified Background setter - 3 Backgrounds
* 1 - Game Background
* 2 - Victory Background
* 0 - DEFEAT
*/
void setup_background(int i) {

    memcpy16_dma((unsigned short*) bg_palette, (unsigned short*) background_palette, PALETTE_SIZE);

    memcpy16_dma((unsigned short*) char_block(0), (unsigned short*) background_data,
            (background_width * background_height) / 2);

    /* set all control the bits in this register */
    *bg0_control = 0 |    /* priority, 0 is highest, 3 is lowest */
        (0 << 2)  |       /* the char block the image data is stored in */
        (0 << 6)  |       /* the mosaic flag */
        (1 << 7)  |       /* color mode, 0 is 16 colors, 1 is 256 colors */
        (16 << 8) |       /* the screen block the tile data is stored in */
        (1 << 13) |       /* wrapping flag */
        (0 << 14);        /* bg size, 0 is 256x256 */

    if(i == 1){
    memcpy16_dma((unsigned short*) screen_block(16), (unsigned short*) map, map_width * map_height);
    }else if(i == 2){
    memcpy16_dma((unsigned short*) screen_block(16), (unsigned short*) map2, map2_width * map2_height);
    }else
    memcpy16_dma((unsigned short*) screen_block(16), (unsigned short*) map3, map3_width * map3_height);

}

//Delay - Used for game reset / action delays
void delay(unsigned int amount) {
    for (int i = 0; i < amount * 10; i++);
}

//Sprite setup
struct Sprite {
    unsigned short attribute0;
    unsigned short attribute1;
    unsigned short attribute2;
    unsigned short attribute3;
};

/* array of all the sprites available on the GBA */
struct Sprite sprites[NUM_SPRITES];
int next_sprite_index = 0;

/* the different sizes of sprites which are possible */
enum SpriteSize {
    SIZE_8_8,
    SIZE_16_16,
    SIZE_32_32,
    SIZE_64_64,
    SIZE_16_8,
    SIZE_32_8,
    SIZE_32_16,
    SIZE_64_32,
    SIZE_8_16,
    SIZE_8_32,
    SIZE_16_32,
    SIZE_32_64
};

/* function to initialize a sprite with its properties, and return a pointer */
struct Sprite* sprite_init(int x, int y, enum SpriteSize size,
        int horizontal_flip, int vertical_flip, int tile_index, int priority) {

    /* grab the next index */
    int index = next_sprite_index++;

    /* setup the bits used for each shape/size possible */
    int size_bits, shape_bits;
    switch (size) {
        case SIZE_8_8:   size_bits = 0; shape_bits = 0; break;
        case SIZE_16_16: size_bits = 1; shape_bits = 0; break;
        case SIZE_32_32: size_bits = 2; shape_bits = 0; break;
        case SIZE_64_64: size_bits = 3; shape_bits = 0; break;
        case SIZE_16_8:  size_bits = 0; shape_bits = 1; break;
        case SIZE_32_8:  size_bits = 1; shape_bits = 1; break;
        case SIZE_32_16: size_bits = 2; shape_bits = 1; break;
        case SIZE_64_32: size_bits = 3; shape_bits = 1; break;
        case SIZE_8_16:  size_bits = 0; shape_bits = 2; break;
        case SIZE_8_32:  size_bits = 1; shape_bits = 2; break;
        case SIZE_16_32: size_bits = 2; shape_bits = 2; break;
        case SIZE_32_64: size_bits = 3; shape_bits = 2; break;
    }

    int h = horizontal_flip ? 1 : 0;
    int v = vertical_flip ? 1 : 0;

    /* set up the first attribute */
    sprites[index].attribute0 = y |             /* y coordinate */
                            (0 << 8) |          /* rendering mode */
                            (0 << 10) |         /* gfx mode */
                            (0 << 12) |         /* mosaic */
                            (1 << 13) |         /* color mode, 0:16, 1:256 */
                            (shape_bits << 14); /* shape */

    /* set up the second attribute */
    sprites[index].attribute1 = x |             /* x coordinate */
                            (0 << 9) |          /* affine flag */
                            (h << 12) |         /* horizontal flip flag */
                            (v << 13) |         /* vertical flip flag */
                            (size_bits << 14);  /* size */

    /* setup the second attribute */
    sprites[index].attribute2 = tile_index |   // tile index */
                            (priority << 10) | // priority */
                            (0 << 12);         // palette bank (only 16 color)*/

    /* return pointer to this sprite */
    return &sprites[index];
}

void sprite_update_all() {
    memcpy16_dma((unsigned short*) sprite_attribute_memory, (unsigned short*) sprites, (NUM_SPRITES * 4));
}

/* setup all sprites */
void sprite_clear() {
    next_sprite_index = 0;

    for(int i = 0; i < NUM_SPRITES; i++) {
        sprites[i].attribute0 = SCREEN_HEIGHT;
        sprites[i].attribute1 = SCREEN_WIDTH;
    }
}

/* set a sprite postion */
void sprite_position(struct Sprite* sprite, int x, int y) {
    /* clear out the y coordinate */
    sprite->attribute0 &= 0xff00;

    /* set the new y coordinate */
    sprite->attribute0 |= (y & 0xff);

    /* clear out the x coordinate */
    sprite->attribute1 &= 0xfe00;

    /* set the new x coordinate */
    sprite->attribute1 |= (x & 0x1ff);
}


void sprite_move(struct Sprite* sprite, int dx, int dy) {
    int y = sprite->attribute0 & 0xff;
    int x = sprite->attribute1 & 0x1ff;
    sprite_position(sprite, x + dx, y + dy);
}

/* change the vertical flip flag */
void sprite_set_vertical_flip(struct Sprite* sprite, int vertical_flip) {
    if (vertical_flip) {
        sprite->attribute1 |= 0x2000;
    } else {
        sprite->attribute1 &= 0xdfff;
    }
}

void sprite_set_horizontal_flip(struct Sprite* sprite, int horizontal_flip) {
    if (horizontal_flip) {
        sprite->attribute1 |= 0x1000;
    } else {
        sprite->attribute1 &= 0xefff;
    }
}

/* change the tile offset of a sprite */
void sprite_set_offset(struct Sprite* sprite, int offset) {
    /* clear the old offset */
    sprite->attribute2 &= 0xfc00;

    /* apply the new one */
    sprite->attribute2 |= (offset & 0x03ff);
}

/* setup the sprite image and palette */
void setup_sprite_image() {
    /* load the palette from the image into palette memory*/
    memcpy16_dma((unsigned short*) sprite_palette, (unsigned short*) megaman_palette, PALETTE_SIZE);

    /* load the image into char block 0 */
    memcpy16_dma((unsigned short*) sprite_image_memory, (unsigned short*) megaman_data, (megaman_width * megaman_height) / 2);
}

//Megaman Struct
struct Megaman {
    struct Sprite* sprite;

//Base sprite functionality
    int x, y;
    int yvel;
    int gravity; 
    int frame;
    int animation_delay;
    int counter;
    int move;
    int border;
    int falling;
//Additional Functionality 
    int attacking;          // Locks megaman into attacking animation
    int moved;              // Checks if megaman actually moved - mainly for x collison / xscroll
    int faceleft;           // Checks what way megaman is facing - x collision
    int ai;                 // Is megaman an ai?

    int damaged;            // Locks into damage animation
    int recdamaged;         // Was he damaged recently? - Iframe calculation
    int taken;              // Damage taken
    int given;              // NPCS killed
};

//Initializing megaman - Note, additional setup is needed for x collision and npc calculations
void megaman_init(struct Megaman* megaman) {
    megaman->x = 80 << 8;
    megaman->y = 90 << 8;
    megaman->yvel = 0;
    megaman->gravity = 50;
    megaman->border = 40;
    megaman->frame = 0;
    megaman->move = 0;
    megaman->moved = 0;
    megaman->faceleft = 0;
    megaman->counter = 0;
    megaman->falling = 0;
    megaman->attacking = 0;
    megaman->ai = 0;
    megaman->damaged = 0;
    megaman->taken = 0;
    megaman ->given = 1;
    megaman->recdamaged = 0;
    megaman->animation_delay = 8;
    megaman->sprite = sprite_init(megaman->x >> 8, megaman->y >> 8, SIZE_16_32, 0, 0, megaman->frame, 0);
}

//Initializing an npc - they don't need as many values to work properly.
void enemy_init(struct Megaman* enemer){
    enemer->yvel = 0;
    enemer->gravity = 50;
    enemer->border = 40;
    enemer->move = 0;
    enemer->counter = 0;
    enemer->falling = 0;
    enemer->ai = 1;
    enemer->animation_delay = 8;
    enemer->sprite = sprite_init((enemer->x >> 8), enemer->y >> 8, SIZE_16_32, 0, 0, enemer->frame, 0);
}

//Megaman movement - left
int megaman_left(struct Megaman* megaman) {
    //Facing left
    megaman->faceleft = 1;
    sprite_set_horizontal_flip(megaman->sprite, 1);
    megaman->move = 1;

    //Ai's move at half speed. Ais can't scroll the screen.
    if ((megaman->x >> 8) < megaman->border && !megaman->ai) {
        return 1;
    } else {
        if(megaman->moved){
            if(!megaman->ai){
            megaman->x -= 256;
            }else
                megaman->x -= 128;

        }
        return 0;
    }
}
//Same as megaman left, under the RIGHT functionality hahahaha
int megaman_right(struct Megaman* megaman) {

    megaman-> faceleft = 0;
    sprite_set_horizontal_flip(megaman->sprite, 0);
    megaman->move = 1;

    if ((megaman->x >> 8) > (SCREEN_WIDTH - 16 - megaman->border ) && !megaman->ai) {
        return 1;
    } else {
        if(megaman->moved){
             if(!megaman->ai){
        megaman->x += 256;
            }else
        megaman->x += 128;
        }
        return 0;
    }
}

/* stop the megaman from walking left/right */
void megaman_stop(struct Megaman* megaman) {
    if(megaman->falling == 1){
        megaman-> frame = 48;
        sprite_set_offset(megaman->sprite, megaman->frame);
    }else{
    megaman->move = 0;
    megaman->frame = 0;
    megaman->counter = 7;
    sprite_set_offset(megaman->sprite, megaman->frame);
}
}

//Jump setup
void megaman_jump(struct Megaman* megaman) {
    if (!megaman->falling) {
        megaman->yvel = -1350;
        megaman->falling = 1;
    }
}

//I saym megashoot, i mean megaslash ;)
void mega_shoot(struct Megaman* megaman){
        if(megaman->attacking < 16){
         megaman->frame = 144;
         megaman->attacking += 1;
        }else{
         megaman->frame = 160;
         megaman-> attacking += 1;
        }

        if(megaman->attacking >= 35){
            megaman->attacking = 0;
        }

        sprite_set_offset(megaman->sprite, megaman->frame);
}

//Finds tiles for x and y collision. This'll be fun.
unsigned short tile_lookup(int x, int y, int xscroll, int yscroll,
        const unsigned short* tilemap, int tilemap_w, int tilemap_h, int lookup) {

    /* adjust for the scroll */
    x += xscroll;
    y += yscroll;

    /* convert from screen coordinates to tile coordinates */
    x >>= 3;
    y >>= 3;

    /* account for wraparound */
    while (x >= tilemap_w) {
        x -= tilemap_w;
    }
    while (y >= tilemap_h) {
        y -= tilemap_h;
    }
    while (x < 0) {
        x += tilemap_w;
    }
    while (y < 0) {
        y += tilemap_h;
    }

    //Checks for the x or the y tile adjacent. Honestly, there's some spaghetti code here.
    //I broke x-collision so many times, I went to remove it. When I removed the code I added, x collision worked.
    int index = 0;
    if(lookup){
    index = y * tilemap_w + x;
    }else{ 
    index = y * tilemap_w + x + 1;
    }
    
    /* return the tile */
    return tilemap[index];
}


//Update megamans position
void megaman_update(struct Megaman* megaman, int xscroll) {

    //Adds a falling / jumping animation, if the struct isn't an npc frame.
    if (megaman->falling) {
        megaman->y += megaman->yvel;
        megaman->yvel += megaman->gravity;
        if(megaman->frame != 112 && megaman->frame != 128 && megaman->frame != 176){
        megaman-> frame = 48;
        }
         sprite_set_offset(megaman->sprite, megaman->frame);
    }

    /* check which tile the megaman's feet are over */
    unsigned short tile = tile_lookup((megaman->x >> 8) + 8, (megaman->y >> 8) + 32, xscroll,
            0, map, map_width, map_height, 1);


    if ((tile >= 7 ) ){
        megaman->falling = 0;
        megaman->yvel = 0;
        megaman->y &= ~0x7ff;
        megaman->y++;
    } else {
        /* he is falling now */
        megaman->falling = 1;
    }

    //Here we go. We're checking the x tiles
    //If he's facing left, we're going to check the LEFT most tile, otherwise, the right
    if(!megaman->faceleft){
    tile = tile_lookup((megaman->x >> 8) + 8, (megaman->y >> 8), xscroll,
            0, map, map_width, map_height, 0);
    }else{
        tile = tile_lookup((megaman->x >> 8) - 8, (megaman->y >> 8), xscroll,
            0, map, map_width, map_height, 0);
    }

    //Moving animation frames. Edited to include additional, smoother movement.
    if(!megaman->ai){
    if (megaman->move && megaman->falling != 1) {
        megaman->counter++;
        if (megaman->counter >= megaman->animation_delay) {
            megaman->frame = megaman->frame + 16;
            if (megaman->frame > 32) {
                megaman->frame = 16;
            }
            sprite_set_offset(megaman->sprite, megaman->frame);
            megaman->counter = 0;
        }
    }
    }else{
        sprite_set_offset(megaman->sprite, megaman->frame);
    }

    //Checks if megaman actually moved, without messing up the moving animation.
    //If he didn't the xscroll isn't changed.
    if((tile <= 7)){
    sprite_position(megaman->sprite, megaman->x >> 8, megaman->y >> 8);
    megaman->moved = 1;
    }else
        megaman->moved = 0;
}

//Is megaman in attack animation?
int attacking(struct Megaman* megaman){
    if(megaman->attacking > 0){
        return 1;
    }else
        return 0;
}

//Setting up an enemies frame / location
void set_enemy(struct Megaman* enemy, int type){
    if(type == 2){
        enemy->frame = 112;
        enemy->x = 100 << 8;
        enemy->y = 10 << 8;
    }else{
        enemy->frame = 128;
        enemy->x = 148 << 8;
        enemy->y = 40 << 8;
    }
}

//Did megaman move?
int moved (struct Megaman* megaman){
    return megaman-> moved;
}


//Display damage frame for a couple seconds.
void damage(struct Megaman* megaman){
    if(megaman->damaged < 40){

        if(megaman->damaged == 1){
            play_sound(damagery, damagery_bytes, 16000, 'B');
        }
         megaman->frame = 80;
         megaman->damaged += 1;
         megaman->recdamaged = 80;
        }else{
            megaman->damaged = 0;
        }

sprite_set_offset(megaman->sprite, megaman->frame);
}

//Count down iframes
void iframe(struct Megaman* player){
    if(player->recdamaged > 0){
    player->recdamaged -= 1;
    }
}

//Is megaman currently in damage frame?
int damaging(struct Megaman* player){
     if(player->damaged > 0){
        return 1;
    }else
        return 0;
}

//Is megaman in iframe?
int recdamaging(struct Megaman* player){
     if(player->recdamaged > 0){
        return 1;
    }else
        return 0;
}

//Checks nearby collision with npcs
//Note - we're only checking lefts and rightmost parts of the collision, as it slows the game otherwise.
int collision (struct Megaman* player, struct Megaman* ai){

    //Checking the left of the npc hitbox
    if((player->x -2048) == (ai->x) && player->y == ai->y && ai->frame < 176){
        if(player->attacking){
            ai->frame = 176;
            player->given +=1;
        }else{
        damage(player);
        player->taken +=1;
        }   
    }else{
        //Checking the right of the npc hitbox.
        if((player->x +2048) == (ai->x) && player->y == ai->y && ai->frame < 176){
        if(player->attacking){
            ai->frame = 176;
            player->given +=1;
        }else{
        damage(player);
        player->taken +=1;
        }
    }
    }
}

//Has megaman taken 3 hits? If so - gameover.

int checkery(int tocheck);

int GAMEOVER(struct Megaman* player){
    int tocheck = checkery(player->taken);
    
    return tocheck;
}

//Has megaman killed the right amount of npcs? if so - victory!
int GAMEWIN(struct Megaman* player){
    int tocheck = checkery(player->given);


    return tocheck;
}


int main() {
    *display_control = MODE0 | BG0_ENABLE | SPRITE_ENABLE | SPRITE_MAP_1D;

    *interrupt_enable = 0;
    *interrupt_callback = (unsigned int) &on_vblank;
    *interrupt_selection |= INTERRUPT_VBLANK;
    *display_interrupts |= 0x08;
    *interrupt_enable = 1;


    *sound_control = 0;

    play_sound(battletheme, battletheme_bytes, 16000, 'A');

    setup_background(1);

    setup_sprite_image();
    sprite_clear();

//Setting up npcs
    struct Megaman megaman;
    megaman_init(&megaman);

    struct Megaman enemy2;
    struct Megaman enemy1;

    set_enemy(&enemy2, 2);
    set_enemy(&enemy1, 1);

    enemy_init(&enemy1);
    enemy_init(&enemy2);

    int xscroll = 0;
    int count = 0;
    int shooting = 0;
    int enemyai = 0;
    /* loop forever */

    /*LOOP action priority
    * 1 - Damage 
    * 2 - Attack 
    * 3 - Movement 
    * 4 - Misc checks / counter updates
    */
    while (1) {
        //Refresh all npcs.
        megaman_update(&megaman, xscroll);
        megaman_update(&enemy2, xscroll);
        megaman_update(&enemy1, xscroll);

        if(damaging(&megaman)){
            damage(&megaman);

        }else{
        if(attacking(&megaman)){
         mega_shoot(&megaman);

        }else{
        if (button_pressed(BUTTON_RIGHT)) {
            if (megaman_right(&megaman) && moved(&megaman)) {
                xscroll++;
            }
        } else if (button_pressed(BUTTON_LEFT)) {
            if (megaman_left(&megaman) && moved(&megaman)) {
                xscroll--;
            }
        } else {
            megaman_stop(&megaman);
        }

        /* check for jumping */
        if (button_pressed(BUTTON_A)) {
            megaman_jump(&megaman);
        }

         if (button_pressed(BUTTON_B)) {
            mega_shoot(&megaman);
            play_sound(sword, sword_bytes, 16000, 'B');
        }
        }
        }

        //Bad AI movement
        if(enemyai){
            megaman_left(&enemy1);
            megaman_right(&enemy2);
        }else{
            megaman_right(&enemy1);
            megaman_left(&enemy2);
        }

        count += 1;
        if(count > 40 && enemyai){
            enemyai = 0;
            count = 0;
        }

        if(count > 40 && !enemyai){
            enemyai = 1;
            count = 0;
        }

        //Bad ai movement end

        //Counting down iframes, if he has none, check collisions
        if(!recdamaging(&megaman)){
        collision(&megaman, &enemy1);
        collision(&megaman, &enemy2);
        }

        iframe(&megaman);

        //Game end triggers
        if(GAMEOVER(&megaman) && !attacking(&megaman)){
            sprite_clear();
            sprite_update_all();
            setup_background(0);
            delay(120000);
            break;
        }

        if(GAMEWIN(&megaman) && !attacking(&megaman)){
            sprite_clear();
            sprite_update_all();
            setup_background(2);
            delay(120000);
            break;
        }

        //Screen resets
        wait_vblank();
        *bg0_x_scroll = xscroll;
        sprite_update_all();

        /* delay some */
        delay(300);
    }
}


void interrupt_ignore() {
}

/* this table specifies which interrupts we handle which way
 * for now, we ignore all of them */
typedef void (*intrp)();
const intrp IntrTable[13] = {
    interrupt_ignore,   /* V Blank interrupt */
    interrupt_ignore,   /* H Blank interrupt */
    interrupt_ignore,   /* V Counter interrupt */
    interrupt_ignore,   /* Timer 0 interrupt */
    interrupt_ignore,   /* Timer 1 interrupt */
    interrupt_ignore,   /* Timer 2 interrupt */
    interrupt_ignore,   /* Timer 3 interrupt */
    interrupt_ignore,   /* Serial communication interrupt */
    interrupt_ignore,   /* DMA 0 interrupt */
    interrupt_ignore,   /* DMA 1 interrupt */
    interrupt_ignore,   /* DMA 2 interrupt */
    interrupt_ignore,   /* DMA 3 interrupt */
    interrupt_ignore,   /* Key interrupt */
};

