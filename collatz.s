@ collatz.s

/* function to return the number of steps in the collatz sequence a number produces */
.global	collatz

collatz:

@ step counter   
mov r1, #0

 .top:
 @ compare our number to 1, skip to the end if true
 	cmp r0, #1
 	beq .done

@ Check if r0 is odd by comparing the last digit, then storing the compare in r2
 	and r2, r0, #1
 	cmp r2, #0
 	bne .odd

@Divide by two if even, add one to counter
.even:
	mov r0, r0, LSR #1
	add r1, r1, #1
	b .top

@Multiply by 3, then add one to the counter and to the result
.odd:
	mov r3, r0
	add r0, r0, r0
	add r0, r0, r3
	
	add r0, r0, #1
	add r1, r1, #1
	b .top


.done:
	@R0 is being returned, so the counter is being swapped
	mov r0, r1
    mov pc, lr

